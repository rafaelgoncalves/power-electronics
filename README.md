# Power Electronics with SPICE

Simulation exercises for EE833 (1s2020) on power electronics at Unicamp

## Index of experiments
 1. Uncontrolled rectifier (diode rectifier)
 1. Controlled rectifier (tyristor rectifier)
 1. TRIAC
 1. Diodes and transistors
 1. DC-DC converters 1
 1. DC-DC converters 2
 1. Inverters

## How to run simulations

Simulations - stored as `.cir` ngspice files - can be run with `./run_simulations.sh` script that are inside `src/<n. of experiment>`. This will generate data on `src/<n. of experiment>/data` folder. Reports with plots and comments in portuguese are stored as `.ipynb` in `src` and as `.pdf` in `doc`.

One can use the Makefile in root for automatize this process:

 - `make ex=<n>` convert `.ipynb` file at `src/<n>/` into a `.pdf` report in `doc/`
 - `make doc ex=<n>` same as above
 - `make run ex=<n>` run ngspice simulations of exercise n
 - `make clean-data ex=<n>` clean output data in `src/<n>/data`
 - `make clean` clean `doc/` folder

