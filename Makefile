ex?=01
srcd=src
docd=doc
datad=$(src)/$(ex)/data
doc_src=$(srcd)/$(ex)/*.ipynb
run_script=$(srcd)/$(ex)/run_simulations.sh
doc_ext=pdf
template=$(srcd)/hidecode.tplx
nbconvert=jupyter nbconvert
nbconvert_opt=--to $(doc_ext) --template $(template) --output-dir $(docd) --execute

doc: run
	$(nbconvert) $(nbconvert_opt) $(doc_src)

run: clean-data
	-$(run_script)

clean-data:
	rm -f $(datad)/*

clean:
	rm -f $(docd)/*

.PHONY: doc run clean clean-data
