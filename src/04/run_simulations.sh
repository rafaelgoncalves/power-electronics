#! /bin/sh
cd `dirname $0`

ngspice -b ex1.cir netlist.cir ../models/schottky.cir > data/01.out 2>&1
ngspice -b ex2.cir netlist.cir ../models/schottky.cir > data/02.out 2>&1
ngspice -b ex3.cir netlist.cir ../models/schottky.cir > data/03.out 2>&1
ngspice -b ex4.cir netlist2.cir ../models/nigbt.cir > data/04.out 2>&1
ngspice -b ex5.cir netlist2.cir ../models/nigbt.cir > data/05.out 2>&1
ngspice -b ex6.cir netlist2.cir ../models/nigbt.cir > data/06.out 2>&1
ngspice -b ex7.cir netlist2.cir ../models/nigbt.cir > data/07.out 2>&1
