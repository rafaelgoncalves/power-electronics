#! /bin/sh
cd `dirname $0`

ngspice -b ex1.cir netlist.cir > data/01.out 2>&1
ngspice -b ex2.cir netlist1.cir > data/02.out 2>&1
ngspice -b ex3.cir netlist2.cir > data/03.out 2>&1
ngspice -b ex4.cir netlist3.cir ../models/1n4002.cir > data/04.out 2>&1
