#! /bin/sh
cd `dirname $0`

ngspice -b ex1.cir netlist.cir ../models/1n4002.cir ../models/scr.cir > data/01.out 2>&1
ngspice -b ex2.cir netlist.cir ../models/1n4002.cir ../models/scr.cir > data/02.out 2>&1
ngspice -b ex3.cir netlist2.cir ../models/1n4002.cir ../models/scr.cir > data/03.out 2>&1
