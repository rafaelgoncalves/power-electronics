#! /bin/sh
cd `dirname $0`

ngspice -b ex1.cir netlist.cir ../models/triac.cir > data/01.out 2>&1
ngspice -b ex2.cir netlist2.cir ../models/triac.cir > data/02.out 2>&1
ngspice -b ex3.cir netlist3.cir ../models/triac.cir > data/03.out 2>&1
ngspice -b ex4.cir netlist4.cir ../models/triac.cir > data/04.out 2>&1
ngspice -b ex5.cir netlist5.cir ../models/triac.cir > data/05.out 2>&1
